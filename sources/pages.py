# -*- coding: utf-8 -*-
"""
    Copyright (C) 2018  Antoine Combet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
# import json
import save_interface as sv_if
from workdata import FOLDER_PATH, VIEWS_PATH, EST_MER, AVEC_VILLE, RACES, UTYPE, UEQUIP, RAMNGT, BTYPE
import json
import cherrypy as cp

SV_PATH = FOLDER_PATH + "/save.json"


def setup():
    try:
        fi = open(SV_PATH, "x")
        terrlist = []
        for i in range(424):
            terrlist.append({
                "owner": -1,
                "city": i in AVEC_VILLE,
                "sea": i in EST_MER,
                "fittings": []
            })
        json.dump({"players": [], "units": [], "terrain": terrlist}, fi, indent=2)
        fi.close()
    except FileExistsError:
        pass
    print("""Copyright (C) 2018  Antoine Combet
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License at <https://www.gnu.org/licenses/>
for more details.""")


class Pages(object):

    def __init__(self):
        self.save_interface = sv_if.SaveInterface(SV_PATH)

    def header(title = ""):
        return """<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - {0}</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>{0}</h2>
    </header>
""".format(title)

    @cp.expose(alias=["index.html"])
    def index(self):
        return open(VIEWS_PATH + "/index.html")

    @cp.expose(alias=["map.html"])
    def map(self):
        return open(VIEWS_PATH + "/map.html")

    @cp.expose(alias=["add_player.html"])
    def add_player(self):
        return open(VIEWS_PATH + "/add_player.html")

    @cp.expose(alias=["player_added.html"])
    def player_added(self, name, race):
        start = self.save_interface.get_free_starting_terrain()
        pid = self.save_interface.add_player(int(race), start, name, 0)
        return """
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - Joueur ajout&eacute;</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Joueur ajout&eacute;</h2>
    </header>
    <p>Le joueur {0} a ete ajout&eacute;, il possede l'ID num&eacute;ro {1}.</p>
    <p>Il jouera les {2}. Il commence sur la region num&eacute;ro {3}</p>
    <br />
    <br />
    <br />
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>
""".format(name, pid, RACES[int(race)], start)

    @cp.expose(alias=["add_unit.html"])
    def add_unit(self):
        return open(VIEWS_PATH + "/add_unit.html")

    @cp.expose(alias=["list_players.html"])
    def list_players(self):
        player_list = self.save_interface.get_player_list()
        table = ""
        for i in range(len(player_list)):
            table = table + """<tr>
<td>{0}</td>
<td>{1}</td>
<td>{2}</td>
<td>{3}</td>
</tr>
""".format(i, player_list[i]["name"], RACES[player_list[i]["race"]]["namePlayer"], player_list[i]["money"])
            pass
        return (self.header("Liste des joueurs") +
            """<table>
<tr>
<th>N°</th>
<th>Nom</th>
<th>Race</th>
<th>Solde</th>
</tr>
""" +
        
"""    </table>
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>
""")

    @cp.expose(alias=["unit_added.html"])
    def unit_added(self, pid, tid, utype):
        tow = self.save_interface.get_terrain_owner(int(tid))
        if tow == int(pid):
            uid = self.save_interface.add_unit(int(pid), int(utype), int(tid))
            out = """<html>
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - Unit&eacute; ajout&eacute;e</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Unit&eacute; ajout&eacute;e</h2>
    </header>
    <p>L'unit&eacute; a bien &eacute;t&eacute; cr&eacute;&eacute;e</p>
    <p>Elle porte l'ID num&eacute;ro {0}</p>
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>""".format(str(uid))
        else:
            out = """<html>
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - Ajout impossible</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Ajout Impossible</h2>
    </header>
    <p>L'unit&eacute; n'a pas pu &ecirc;tre cr&eacute;&eacute;e</p>
    <p>La r&eacute;gion {0} n'appartient pas au joueur {1}</p>
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>""".format(str(tid), str(pid))
        return out

    @cp.expose(alias=["change_reg_owner.html"])
    def change_reg_owner(self):
        return open(VIEWS_PATH + "/change_reg_owner.html")

    @cp.expose(alias=["reg_owner_changed.html"])
    def reg_owner_changed(self, pid, tid):
        self.save_interface.set_terrain_owner(int(tid), int(pid))
        return """<html>
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - Modification &Eacute;fectu&eacute;e</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Modification &Eacute;fectu&eacute;e</h2>
    </header>
    <p>La r&eacute;gion num&eacute;ro {0} appartient d&eacute;sormais au joueur {1}</p>
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>
""".format(tid, pid)

    @cp.expose(alias=["delete_unit.html"])
    def delete_unit(self):
        return open(VIEWS_PATH + "/delete_unit.html")

    @cp.expose(alias=["unit_deleted.html"])
    def unit_deleted(self, uid):
        self.save_interface.del_unit(int(uid))
        return """
<html lang='fr'>
<head>
    <meta charset='UTF-8' />
    <link rel='stylesheet' href='/static/style.css'>
    <title>Gestion de campagne - Unit&eacute; supprime&eacute;</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Unit&eacute; supprime&eacute;</h2>
    </header>
    <p>L'unit&eacute; num&eacute;ro {0} a &eacute;t&eacute; supprim&eacute;</p>
    <center><p class='home'><a href='/'>Menu principal</a></p></center>
</body>
</html>""".format(uid)

    @cp.expose(alias=["edit_money.html"])
    def edit_money(self):
        return open(VIEWS_PATH + "/edit_money.html")

    @cp.expose(alias=["money_edited.html"])
    def money_edited(self, pid, new_money):
        self.save_interface.set_money(int(pid), int(new_money))
        return """<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel='stylesheet' href='/static/style.css' />
    <title>Gestion de campagne - Solde modifi&eacute;</title>
</head>
<body>
    <header>
        <h1>Gestion de campagne</h1>
        <h2>Solde modifi&eacute;</h2>
    </header>
    <p>Le solde du joueur {0} a &eacute;t&eacute; modifi&eacute;.</p>
    <p>Il est désormais de {1}.</p>
    <center><p><a href="/">Menu Principal</a></p></center>
</body>
</html>""".format(pid, new_money)

    @cp.expose(alias=["change_unit_wpn.html"])
    def change_unit_wpn(self):
        return open(VIEWS_PATH + "/change_unit_wpn.html")

    @cp.expose(alias=["unit_wpn_changed.html"])
    def unit_wpn_changed(self):
        return """<html>
    <head>
        <meta charset='UTF-8' />
        <link rel="stylesheet" href="/static/style.css" />
        <title>Gestion de camagne - Arme de l'unit&eacute; modifi&eacute;</title>
    </head>
    <body>
        <header>
            <h1>Gestion de camagne</h1>
            <h2>Arme de l'unit&eacute; modifi&eacute;</h2>
        </header>
        <center><p class='home'><a href='/'>Menu Principal</a></p></center>
    </body>
    </html>"""

# -*- coding: utf-8 -*-
"""
    Copyright (C) 2018  Antoine Combet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
"""
import random
import json

# MNEMONICS DEFINITION
TERRAIN = "terrain"
PLAYER  = "players"
UNIT    = "units"

class SaveInterface(object):
    """
    This object is meant to manage the relation between the program and the database
    """

    def __init__(self, path):
        """
        Links the object to the database
        :param path: database path
        """
        self.SV_PATH = path

    def load(self):
        with open(self.SV_PATH, "r") as fi:
            save = json.load(fi)
        return save

    def update(self, save: dict):
        with open(self.SV_PATH, "w") as fi:
            json.dump(save, fi, indent = 2)
        return 0

    def get_player_list(self):
        """
        :return: list of registered players
        """
        return self.load()[PLAYER]

    def add_player(self, race: int, start: int, name="", money=0) -> int:
        """
        Registers a new player to the database
        :param race: player's race
        :param start: starting region
        :param name: player's name , default <"">
        :param money: player starting money , default <0>
        :return: new player ID
        """
        save = self.load()
        save[PLAYER].append({"name": name, "race": race, "money": money})
        pid = len(save[PLAYER]) - 1
        save[TERRAIN][start]["owner"] = pid
        self.update(save)
        return pid

    def set_money(self, pid: int, new_money: int):
        """
        Changes player's money
        :param pid: ID of the concerned player
        :param new_money: the new amount of money this player will have
        """
        save = self.load()
        save[PLAYER][pid]["money"] = new_money
        self.update(save)

    def get_player_unit_list(self, pid: int) -> list:
        """
        Gets the unit list of a player
        :param pid: player's ID
        :return: unit list
        """

        save = self.load()
        out = list()
        for ele in save[UNIT]:
            if ele["owner"] == pid:
                out.append(ele)
        return out

    def add_unit(self, pid: int, typ: int, location: int):
        """
        Create a new unit
        :param pid: unit owner's ID
        :param typ: unit type
        :param location: unit location
        :return: new unit's ID
        """
        save = self.load()
        save["units"].append({
            "type": typ,
            "owner": pid,
            "loc": location,
            "wpns": [-1],
            "equip": [],
            "training": 0
        })
        uid = len(save["units"]) - 1
        self.update(save)
        return uid

    def del_unit(self, uid: int):
        """
        Remove an unit
        :param uid: to be removed unit's ID
        """
        save = self.load()
        save["units"].remove(save["units"][uid])
        self.update(save)

    def get_unit_info(self, uid: int):
        """
        get info about a specific unit
        :param uid: unit's ID
        :return: unit's info
        """
        save = self.load()
        return save["units"][uid]

    def add_to_unit_equipment(self, uid: int, new_equip: int):
        """
        Change unit's equipment
        :param uid: unit's ID
        :param new_equip: unit's new equipment
        """
        save = self.load()
        save[UNIT][uid]["equip"].append(new_equip)
        self.update(save)

    def add_to_unit_wpns(self, uid: int, new_weapon: int):
        """
        Change unit's weapon
        :param uid: unit's ID
        :param new_weapon: unit's new weapon
        :return: 0
        """
        save = self.load()
        save[UNIT][uid]["wpns"].append(new_weapon)
        self.update(save)

    def set_unit_loc(self, uid: int, new_loc: int):
        """p
        Change unit's location
        :param uid: unit's ID
        :param new_loc: terrain ID
        :return: 0
        """
        save = self.load()
        save[UNIT][uid]["loc"] = new_loc
        self.update(save)

    def set_terrain_owner(self, tid: int, pid: int):
        """
        Changes terrain owner
        :param tid: terrain ID
        :param pid: owner ID
        :return: 0
        """
        save = self.load()
        save[TERRAIN][tid]["owner"] = pid
        self.update(save)

    def add_to_terrain_fittings(self, tid: int, new_fittings: str):
        """
        Changes terrain fittings
        :param tid: terrain ID
        :param new_fittings: fittings description string
        :return:
        """
        save = self.load()
        save[TERRAIN][tid]["fittings"].append(new_fittings)
        self.update(save)

    def get_free_starting_terrain(self):
        """

        :return:
        """
        save = self.load()
        ftl = list()
        for index in range(len(save[TERRAIN])):
            if save[TERRAIN][index]["owner"] == -1:
                ftl.append(index)

        return random.choice(ftl)

    def get_terrain_owner(self, tid: int):
        return self.load()[TERRAIN][tid]["owner"]

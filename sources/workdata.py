# -*- coding:utf-8 -*
"""
    Constants and config import module

    Copyright (C) 2018  Antoine Combet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
import json



FOLDER_PATH = os.getcwd()

VIEWS_PATH = FOLDER_PATH + "/sources/views"

EST_MER = [76, 78, 80, 77, 79, 81, 82, 150, 180, 146, 179, 170, 178, 169, 168, 88, 89, 90,
           91, 420, 52, 51, 13, 14, 16, 251, 252, 250, 248, 249, 247, 263, 311, 312, 313,
           314, 315, 316, 317, 318, 334, 339, 330, 331, 332, 333, 335, 336, 338, 340, 359,
           355, 354, 356, 357, 360, 361, 362, 24, 22, 36, 38, 19, 18, 37, 108, 109, 125, 126,
           194, 193, 211, 212, 192, 215, 189, 217, 223, 227, 228, 187, 135, 107, 131, 120,
           127, 129, 191, 190, 133, 188, 134, 36, 27, 25, 26, 28, 34, 117, 32, 33, 239, 421,
           226, 341, 342, 343, 350, 347, 348, 351, 352, 353, 210, 205, 208, 204, 203, 209,
           344, 345, 346]

AVEC_VILLE = [1, 8, 31, 40, 47, 50, 54, 62, 69, 92, 98, 105, 111, 114,
              116, 121, 138, 144, 148, 152, 159, 165, 166, 176, 202,
              206, 221, 232, 234, 240, 259, 264, 268, 273, 285, 290,
              298, 302, 310, 324, 327, 349, 358, 363, 366, 371, 382,
              386, 394, 397, 401, 410, 422, 423]

fi = open(FOLDER_PATH + "/config/race.json", "r")
RACES = json.load(fi)["races"]
fi.colse()
fi = open(FOLDER_PATH + "/config/units.json", "r")
UTYPE = json.load(fi)["units"]
UEQUIP = json.load(fi)["stuff"]
fi = open(FOLDER_PATH + "/config/coresp_builds.json", "r")
DICO_RAMNGT = json.load(fi)
fi.close()
DICO_BTYPE = {0: "Barque", 1: "Galère", 2: "Trirème", }



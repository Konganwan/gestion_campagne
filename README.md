Gestion de Campagnes
=========================

Description des fichiers
------------------------

Le répertoire `/config` contient les fichiers de configuration, au format JSON.

Le fichier `/save.json` contient la sauvegarde courante, ici une sauvegarde vide

Le repertoire `/public` contient les images et les feuilles de style.

Le répertoire `/sources` contient les sources du programme réparties ainsi :

  - dans le repertoire `/sources/views` se trouvent les pages HTML
  
  - directement dans `/sources` se trouvent les scripts python formant le programme

  
Mise en place et lancement
--------------------------

### Sous GNU/Linux

Sous la pluspart des distributions Linux, python 3 est déja installé

il vous faudra tout de meme installer cherrypy via PyPI `pip install CherryPy` 

### Sous MS Windows

Il vous faudra python dans sa version 3 (à trouver ici : [python.org](https://python.org))

ensuite ouvrez une console dans le dossier d'instalation de python et executez `pip install CherryPy`

Voilà votre environement python est prêt

### Lancer l'application

Executez dans un terminal depuis le dossier principal de l'application `python3 ./sources/app.py` sous GNU/Linux ou `python source/app.py`sous MS Windows

Vous pourrez ensuite vous connecter à l'application à l'aide du navigateur de votre choix à l'addresse [127.0.0.1:8080](http://localhost:8080).
# gestion_campagne
